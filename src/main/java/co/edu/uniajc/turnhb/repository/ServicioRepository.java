package co.edu.uniajc.turnhb.repository;

import co.edu.uniajc.turnhb.model.servicioModel;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ServicioRepository extends JpaRepository<servicioModel, Long>{
    List<servicioModel> findAllByNamesContains(String servicio);
    servicioModel getById(int id);
    @Query(nativeQuery = true, value="SELECT"+
    "id"+",servicio" + "FROM Servicio"+" WHERE id =: id")
    List<servicioModel> findService(@Param(value = "id") Integer id);
}


